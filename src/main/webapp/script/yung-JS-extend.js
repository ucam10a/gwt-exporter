    // define script folder path
    var _script_root = "./script/";
    var yung_global_var = {
        _global_ajaxTimeout : null
    };
    
    /* Simple JavaScript Inheritance
     * By John Resig http://ejohn.org/
     * MIT Licensed.
     */
    // Inspired by base2 and Prototype
    (function(){
      var initializing = false, fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/;
     
      // The base AbstractClass implementation (does nothing)
      this.AbstractClass = function(){};
     
      // Create a new AbstractClass that inherits from this class
      AbstractClass.extend = function(prop) {
        var _super = this.prototype;
       
        // Instantiate a base class (but only create the instance,
        // don't run the init constructor)
        initializing = true;
        var prototype = new this();
        initializing = false;
       
        // Copy the properties over onto the new prototype
        for (var name in prop) {
          // Check if we're overwriting an existing function
          prototype[name] = typeof prop[name] == "function" &&
            typeof _super[name] == "function" && fnTest.test(prop[name]) ?
            (function(name, fn){
              return function() {
                var tmp = this._super;
               
                // Add a new ._super() method that is the same method
                // but on the super-class
                this._super = _super[name];
               
                // The method only need to be bound temporarily, so we
                // remove it when we're done executing
                var ret = fn.apply(this, arguments);        
                this._super = tmp;
               
                return ret;
              };
            })(name, prop[name]) :
            prop[name];
        }
       
        // The dummy class constructor
        function AbstractClass() {
          // All construction is actually done in the init method
          if ( !initializing && this.init )
            this.init.apply(this, arguments);
        }
       
        // Populate our constructed prototype object
        AbstractClass.prototype = prototype;
       
        // Enforce the constructor to be what we expect
        AbstractClass.prototype.constructor = $Class;
     
        // And make this class extendable
        AbstractClass.extend = arguments.callee;
        
        return AbstractClass;
      };
    })();
    
    /**
     * Basic Class
     * Yung Long Li
     */
    var $Class = AbstractClass.extend({
    	// for reflection
    	objName : null,
    	setObjName : function (name) {
        	this.objName = name;
        	return this;
        },
        debugPrint: function(){
            var result = "object: " + objName + "\n";
            for (var key in this) {
                if (!(typeof(this[key]) === typeof(Function) || key == "_super")){
                    var value = this[key];
                    //if(typeof(value) === "undefined"){
                        result = result + "    " + key + ": " + value + "\n";
                    //}
                }
            }
            alert(result);
        }
    });
    
    /**
     * show AJAX loading 
     */
    function ajaxLoadingShow (){
    }
    
    /**
     * hide AJAX loading 
     */
    function ajaxLoadingHide (){
    }
    
    /**
     * my Ajax object
     */
    var AjaxHttp = $Class.extend({
        action : undefined,
        jsonPayload : undefined,
        response : undefined,
        loading : true,
        showError : true,
        type : 'POST',
        dataType : 'json',
        init: function(action, jsonPayload, response){
            this.action = action;
            if (jsonPayload) this.jsonPayload = jsonPayload;
            if (response) this.response = response;
        },
        beforeSend : function(){
        	if (this.loading) yung_global_var._global_ajaxTimeout = setTimeout("ajaxLoadingShow();", 500);
        },
        setLoading : function(showAjaxLoading){
        	this.loading = showAjaxLoading;
        },
        setShowError : function(showErr){
        	this.showError = showErr;
        },
        send : function(){
            if (this.action == null) {
                alert("please define action!");
                return;
            }
            
            // url
            var ajaxurl = this.action;
            //alert(ajaxurl + " ok");
            
            var paramobj = JSON.parse(this.jsonPayload);
            var keys = Object.keys(paramobj);
            var params = "";
            for (var i = 0; i < keys.length; i++) {
            	params = params + keys[i] + "=" + encodeURI(paramobj[keys[i]]) + "&";
            }
            //alert("params ok");
            
            var callback = this.response;
            var self = this;
            
            $.ajax({
                type:'POST',
                dataType: 'json',
                error: function(x, status, error) {
                	if (self.showError) alert("An ajax error occurred: " + status + " Error: " + error);
                },
                url:ajaxurl,
                data:params,
                jsonpCallback: callback,
                success:function(result){
                	return this.jsonpCallback(result, self);
                }
            });
        }
    });
    
    var Class = {
        newInstance : function (type, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8){
            var base = new Object();
            base['_' + type] = window[type].extend({});
            return new base['_' + type](arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
        }
    }
    