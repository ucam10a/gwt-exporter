package com.yung.test;

import com.google.gwt.core.client.JavaScriptObject;

public class SheetHandler {

    public native static void log(Object obj) /*-{
        console.log(obj);
    }-*/;

    public void activeSheetChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void activeSheetChanging(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void buttonClicked(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void cellChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void cellClick(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void cellDoubleClick(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void clipboardChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void clipboardChanging(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void clipboardPasted(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void clipboardPasting(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void columnChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void columnWidthChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void columnWidthChanging(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void commentChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void commentRemoved(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void commentRemoving(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void cultureChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void customFloatingObjectLoaded(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void dragDropBlock(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void dragDropBlockCompleted(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void dragFillBlock(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void dragFillBlockCompleted(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void editChange(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void editEnd(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void editEnding(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void editStarted(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void editStarting(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void editorStatusChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void enterCell(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void floatingObjectChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void floatingObjectRemoved(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void floatingObjectRemoving(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void floatingObjectSelectionChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void invalidOperation(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void leaveCell(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void leftColumnChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void pictureChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void pictureSelectionChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void rangeChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void rangeFiltered(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void rangeFiltering(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void rangeGroupStateChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void rangeGroupStateChanging(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void rangeSorted(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void rangeSorting(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void editEnded(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void rowChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void rowHeightChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void rowHeightChanging(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void selectionChanging(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void selectionChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void sheetNameChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void sheetNameChanging(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void sheetTabClick(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void sheetTabDoubleClick(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void slicerChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void sparklineChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void tableFiltered(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void tableFiltering(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void topRowChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void touchToolStripOpening(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void userFormulaEntered(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void userZooming(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void validationError(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }

    public void valueChanged(JavaScriptObject evt, JavaScriptObject args) {
        log(evt);
        log(args);
    }
    
}
