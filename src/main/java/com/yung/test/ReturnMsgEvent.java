package com.yung.test;

import com.google.gwt.core.client.JavaScriptObject;

public class ReturnMsgEvent {
    
    private SpreadJSComponent spreadjs;
    
    public ReturnMsgEvent(){
    }
    
    public void setSpreadjs(SpreadJSComponent spreadjs) {
        this.spreadjs = spreadjs;
    }
    
    public SpreadJSComponent getSpreadjs() {
        return spreadjs;
    }
    
    public native static String getType(JavaScriptObject obj) /*-{
        return obj.type;
    }-*/;
    
    public void fireEvent(JavaScriptObject evt, JavaScriptObject args){
        SheetHandler.log(evt);
        String type = getType(evt);
        SheetHandler.log(spreadjs.getHandler());
        if (type.equalsIgnoreCase("activeSheetChanged")) {
            spreadjs.getHandler().activeSheetChanged(evt, args);
        } else if (type.equalsIgnoreCase("activeSheetChanging")) {
            spreadjs.getHandler().activeSheetChanging(evt, args);
        } else if (type.equalsIgnoreCase("buttonClicked")) {
            spreadjs.getHandler().buttonClicked(evt, args);
        } else if (type.equalsIgnoreCase("cellChanged")) {
            spreadjs.getHandler().cellChanged(evt, args);
        } else if (type.equalsIgnoreCase("cellClick")) {
            spreadjs.getHandler().cellClick(evt, args);
        } else if (type.equalsIgnoreCase("cellDoubleClick")) {
            spreadjs.getHandler().cellDoubleClick(evt, args);
        } else if (type.equalsIgnoreCase("clipboardChanged")) {
            spreadjs.getHandler().clipboardChanged(evt, args);
        } else if (type.equalsIgnoreCase("clipboardChanging")) {
            spreadjs.getHandler().clipboardChanging(evt, args);
        } else if (type.equalsIgnoreCase("clipboardPasted")) {
            spreadjs.getHandler().clipboardPasted(evt, args);
        } else if (type.equalsIgnoreCase("clipboardPasting")) {
            spreadjs.getHandler().clipboardPasting(evt, args);
        } else if (type.equalsIgnoreCase("columnChanged")) {
            spreadjs.getHandler().columnChanged(evt, args);
        } else if (type.equalsIgnoreCase("columnWidthChanged")) {
            spreadjs.getHandler().columnWidthChanged(evt, args);
        } else if (type.equalsIgnoreCase("columnWidthChanging")) {
            spreadjs.getHandler().columnWidthChanging(evt, args);
        } else if (type.equalsIgnoreCase("commentChanged")) {
            spreadjs.getHandler().commentChanged(evt, args);
        } else if (type.equalsIgnoreCase("commentRemoved")) {
            spreadjs.getHandler().commentRemoved(evt, args);
        } else if (type.equalsIgnoreCase("commentRemoving")) {
            spreadjs.getHandler().commentRemoving(evt, args);
        } else if (type.equalsIgnoreCase("cultureChanged")) {
            spreadjs.getHandler().cultureChanged(evt, args);
        } else if (type.equalsIgnoreCase("customFloatingObjectLoaded")) {
            spreadjs.getHandler().customFloatingObjectLoaded(evt, args);
        } else if (type.equalsIgnoreCase("dragDropBlock")) {
            spreadjs.getHandler().dragDropBlock(evt, args);
        } else if (type.equalsIgnoreCase("dragDropBlockCompleted")) {
            spreadjs.getHandler().dragDropBlockCompleted(evt, args);
        } else if (type.equalsIgnoreCase("dragFillBlock")) {
            spreadjs.getHandler().dragFillBlock(evt, args);
        } else if (type.equalsIgnoreCase("dragFillBlockCompleted")) {
            spreadjs.getHandler().dragFillBlockCompleted(evt, args);
        } else if (type.equalsIgnoreCase("editChange")) {
            spreadjs.getHandler().editChange(evt, args);
        } else if (type.equalsIgnoreCase("editEnd")) {
            spreadjs.getHandler().editEnd(evt, args);
        } else if (type.equalsIgnoreCase("editEnded")) {
            spreadjs.getHandler().editEnded(evt, args);
        } else if (type.equalsIgnoreCase("editEnding")) {
            spreadjs.getHandler().editEnding(evt, args);
        } else if (type.equalsIgnoreCase("editStarted")) {
            spreadjs.getHandler().editStarted(evt, args);
        } else if (type.equalsIgnoreCase("editStarting")) {
            spreadjs.getHandler().editStarting(evt, args);
        } else if (type.equalsIgnoreCase("editorStatusChanged")) {
            spreadjs.getHandler().editorStatusChanged(evt, args);
        } else if (type.equalsIgnoreCase("enterCell")) {
            spreadjs.getHandler().enterCell(evt, args);
        } else if (type.equalsIgnoreCase("floatingObjectChanged")) {
            spreadjs.getHandler().floatingObjectChanged(evt, args);
        } else if (type.equalsIgnoreCase("floatingObjectRemoved")) {
            spreadjs.getHandler().floatingObjectRemoved(evt, args);
        } else if (type.equalsIgnoreCase("floatingObjectRemoving")) {
            spreadjs.getHandler().floatingObjectRemoving(evt, args);
        } else if (type.equalsIgnoreCase("floatingObjectSelectionChanged")) {
            spreadjs.getHandler().floatingObjectSelectionChanged(evt, args);
        } else if (type.equalsIgnoreCase("invalidOperation")) {
            spreadjs.getHandler().invalidOperation(evt, args);
        } else if (type.equalsIgnoreCase("leaveCell")) {
            spreadjs.getHandler().leaveCell(evt, args);
        } else if (type.equalsIgnoreCase("leftColumnChanged")) {
            spreadjs.getHandler().leftColumnChanged(evt, args);
        } else if (type.equalsIgnoreCase("pictureChanged")) {
            spreadjs.getHandler().pictureChanged(evt, args);
        } else if (type.equalsIgnoreCase("pictureSelectionChanged")) {
            spreadjs.getHandler().pictureSelectionChanged(evt, args);
        } else if (type.equalsIgnoreCase("rangeChanged")) {
            spreadjs.getHandler().rangeChanged(evt, args);
        } else if (type.equalsIgnoreCase("rangeFiltered")) {
            spreadjs.getHandler().rangeFiltered(evt, args);
        } else if (type.equalsIgnoreCase("rangeFiltering")) {
            spreadjs.getHandler().rangeFiltering(evt, args);
        } else if (type.equalsIgnoreCase("rangeGroupStateChanged")) {
            spreadjs.getHandler().rangeGroupStateChanged(evt, args);
        } else if (type.equalsIgnoreCase("rangeGroupStateChanging")) {
            spreadjs.getHandler().rangeGroupStateChanging(evt, args);
        } else if (type.equalsIgnoreCase("rangeSorted")) {
            spreadjs.getHandler().rangeSorted(evt, args);
        } else if (type.equalsIgnoreCase("rangeSorting")) {
            spreadjs.getHandler().rangeSorting(evt, args);
        } else if (type.equalsIgnoreCase("rowChanged")) {
            spreadjs.getHandler().rowChanged(evt, args);
        } else if (type.equalsIgnoreCase("rowHeightChanged")) {
            spreadjs.getHandler().rowHeightChanged(evt, args);
        } else if (type.equalsIgnoreCase("rowHeightChanging")) {
            spreadjs.getHandler().rowHeightChanging(evt, args);
        } else if (type.equalsIgnoreCase("selectionChanged")) {
            spreadjs.getHandler().selectionChanged(evt, args);
        } else if (type.equalsIgnoreCase("selectionChanging")) {
            spreadjs.getHandler().selectionChanging(evt, args);
        } else if (type.equalsIgnoreCase("sheetNameChanged")) {
            spreadjs.getHandler().sheetNameChanged(evt, args);
        } else if (type.equalsIgnoreCase("sheetNameChanging")) {
            spreadjs.getHandler().sheetNameChanging(evt, args);
        } else if (type.equalsIgnoreCase("sheetTabClick")) {
            spreadjs.getHandler().sheetTabClick(evt, args);
        } else if (type.equalsIgnoreCase("sheetTabDoubleClick")) {
            spreadjs.getHandler().sheetTabDoubleClick(evt, args);
        } else if (type.equalsIgnoreCase("slicerChanged")) {
            spreadjs.getHandler().slicerChanged(evt, args);
        } else if (type.equalsIgnoreCase("sparklineChanged")) {
            spreadjs.getHandler().sparklineChanged(evt, args);
        } else if (type.equalsIgnoreCase("tableFiltered")) {
            spreadjs.getHandler().tableFiltered(evt, args);
        } else if (type.equalsIgnoreCase("tableFiltering")) {
            spreadjs.getHandler().tableFiltering(evt, args);
        } else if (type.equalsIgnoreCase("topRowChanged")) {
            spreadjs.getHandler().topRowChanged(evt, args);
        } else if (type.equalsIgnoreCase("touchToolStripOpening")) {
            spreadjs.getHandler().touchToolStripOpening(evt, args);
        } else if (type.equalsIgnoreCase("userFormulaEntered")) {
            spreadjs.getHandler().userFormulaEntered(evt, args);
        } else if (type.equalsIgnoreCase("userZooming")) {
            spreadjs.getHandler().userZooming(evt, args);
        } else if (type.equalsIgnoreCase("validationError")) {
            spreadjs.getHandler().validationError(evt, args);
        } else if (type.equalsIgnoreCase("valueChanged")) {
            spreadjs.getHandler().valueChanged(evt, args);
        }
    }
    
}
