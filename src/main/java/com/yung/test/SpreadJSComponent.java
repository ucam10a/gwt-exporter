package com.yung.test;

import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.widgets.HTMLPane;

public class SpreadJSComponent {

    private static final String PREFIX = "spreadjs";
    
    private String id;
    
    private SheetHandler handler;

    public SpreadJSComponent() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public HTMLPane createWidget() {
        HTMLPane iframe = new HTMLPane();
        iframe.setID(PREFIX + id);
        iframe.setWidth100();
        iframe.setHeight100();
        iframe.setShowEdges(true);
        iframe.setContentsURL("spreadjs/spreadjs.html#" + id);
        iframe.setContentsType(ContentsType.PAGE);
        return iframe;
    }

    public SheetHandler getHandler() {
        return handler;
    }

    public void setHandler(SheetHandler handler) {
        this.handler = handler;
    }

    public void setSpreadjsId() {
        callFunction("setSpreadjsId", id, id, null, null, null, null, null, null, null);
    }
    
    public void bindEvents(Object event) {
        callFunction("sheetBind", id, event, null, null, null, null, null, null, null);
    }
    
    public native Object callFunction(String funt, String id, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5, Object arg6, Object arg7, Object arg8) /*-{
        try {
            var srcPath = 'spreadjs/spreadjs.html#' + id;
            var iframeArr = $wnd.document.querySelectorAll("iframe");
            var iframe = null;
            for (var i = 0; i < iframeArr.length; i++) {
                var iframe = iframeArr[i];
                var src = iframe.getAttribute('src');
                if (srcPath == src) {
                    break;
                }
            }
            iframe.contentWindow['util' + id][funt](arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
        } catch (err) {
            alert("callFunction funt: " + funt + " fails, error: " + err);
        }
    }-*/;
    
}
