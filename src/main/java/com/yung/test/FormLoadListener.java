package com.yung.test;

import java.util.ArrayList;
import java.util.List;

import org.timepedia.exporter.client.Export;
import org.timepedia.exporter.client.Exportable;

@Export
public class FormLoadListener implements Exportable {

    private static List<ReturnMsgEvent> events = new ArrayList<ReturnMsgEvent>();
    
    public static void register(ReturnMsgEvent event){
        events.add(event);
    }
    
    public static void fire(String id) {
        for (ReturnMsgEvent event : events) {
            if (id.equalsIgnoreCase(event.getSpreadjs().getId())) {
                event.getSpreadjs().setSpreadjsId();
                event.getSpreadjs().bindEvents(event);
            }
        }
    }

}
