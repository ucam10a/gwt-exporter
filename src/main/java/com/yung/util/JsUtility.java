package com.yung.util;

public class JsUtility {

    public static native void alert(String message) /*-{
        alert(message);
    }-*/;
    
    public static native void log(String message) /*-{
        console.log(message);
    }-*/;
    
    public static native String getElementValue(String id) /*-{
        try {
            return $wnd.$('#' + id).val();
        } catch(e) {
            alert('getElementValue fail: ' + e.message);
        }
    }-*/;

}
