package com.yung.util;

import com.github.nmorel.gwtjackson.client.ObjectMapper;

public class AjaxHttp<T extends CallBack<R>, R> {
    
    private ObjectMapper<R> mapper;
    
    private T callFunction;
    
    public AjaxHttp(){
    }
    
    public AjaxHttp(ObjectMapper<R> mapper, T callFunction) {
        this.mapper = mapper;
        this.callFunction = callFunction;
    }
    
    private native String sendAndGetResponse(String action, String jsonPayload, AjaxHttp<T, R> caller) /*-{
        try {
            var ajaxhttp = $wnd.Class.newInstance('AjaxHttp', action, jsonPayload, function callback(response)
            {
                try {
                    var json = JSON.stringify(response);
                } catch(e) {
                    alert('JSON.stringify fail: ' + e.message);
                }
                caller.@com.yung.util.AjaxHttp::jsonCallBack(Ljava/lang/String;)(json);
            });
            return ajaxhttp.send();
        } catch(e) {
            alert('sendAndGetResponse fail: ' + e.message);
        }
    }-*/;
    
    public void call(String action, String jsonPayload){
        try {
            JsUtility.log(jsonPayload);
            sendAndGetResponse(action, jsonPayload, this);
        } catch (Exception e) {
            JsUtility.alert(e.toString());
        }
    }
    
    public void jsonCallBack(String json){
        try {
            JsUtility.log("json: " + json);
            R r = mapper.read(json);
            callFunction.call(r);
        } catch (Exception e) {
            JsUtility.alert(e.toString());
        }
    }
    
    
}
