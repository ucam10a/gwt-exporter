package com.yung.util;

public interface CallBack<R> {

    public void call(R response);
    
}
