package com.hellogwt.server;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.hellogwt.client.Person;

public class AjaxServlet extends HttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = -2511101004402700324L;

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        doPost(req, resp);
    }
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        
        String name = req.getParameter("name");
        System.out.println("name: " + name);
        String address = req.getParameter("address");
        System.out.println("address: " + address);
        String ageStr = req.getParameter("age");
        System.out.println("ageStr: " + ageStr);
        int age = Integer.valueOf(ageStr);
        
        Person p = new Person();
        p.setName(name);
        p.setAddress(address);
        p.setAge(age);
        
        Gson gson = new Gson();
        String json = gson.toJson(p);
        PrintWriter writer = resp.getWriter();
        writer.print(json);
        writer.flush();
    
    }
    
}
