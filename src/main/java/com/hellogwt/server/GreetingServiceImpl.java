package com.hellogwt.server;

import com.hellogwt.client.GreetingService;
import org.springframework.stereotype.Service;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@Service("greetingService")
public class GreetingServiceImpl extends RemoteServiceServlet implements GreetingService {

    /**
     * 
     */
    private static final long serialVersionUID = -2329899881999907949L;

    @Override
    public String greet(String name) {
        return "Hello, " + name + "!";
    }
}
