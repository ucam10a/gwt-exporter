package com.hellogwt.client;

import org.timepedia.exporter.client.Export;
import org.timepedia.exporter.client.Exportable;

import com.google.gwt.core.shared.GWT;
import com.yung.util.AjaxHttp;
import com.yung.util.JsUtility;

@Export
public class MyUtilityClass implements Exportable {

    public static void ajaxTest() {
        JsUtility.log("start 1 ...");
        PersonMapper mapper = GWT.create(PersonMapper.class);
        RequestToMapper requestMapper = GWT.create(RequestToMapper.class);
        RequestTo req = new RequestTo();
        req.setName(JsUtility.getElementValue("showPersonForm-name"));
        req.setAddress(JsUtility.getElementValue("showPersonForm-address"));
        req.setAge(JsUtility.getElementValue("showPersonForm-age"));
        String jsonPayload = requestMapper.write(req);
        JsUtility.log("start 2 ...");
        AjaxHttp<ShowPersonCallBack, Person> http = new AjaxHttp<ShowPersonCallBack, Person>(mapper, new ShowPersonCallBack());
        JsUtility.log("start 3 ...");
        http.call("showPerson", jsonPayload);
        JsUtility.log("start 4 ...");
    }
    
}
