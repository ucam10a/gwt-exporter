package com.hellogwt.client;

import com.yung.util.CallBack;
import com.yung.util.JsUtility;

public class ShowPersonCallBack implements CallBack<Person> {

    @Override
    public void call(Person person) {
        JsUtility.log("call start ...");
        String name = person.getName();
        String address = person.getAddress();
        int age = person.getAge();
        JsUtility.alert("Person name: " + name + ", address: " + address + ", age: " + age);        
    }
    
}
