package com.hellogwt.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.smartgwt.client.widgets.layout.VLayout;
import com.yung.test.FormLoadListener;
import com.yung.test.ReturnMsgEvent;
import com.yung.test.SheetHandler;
import com.yung.test.SpreadJSComponent;

public class HelloGWT implements EntryPoint {
    
    private GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
    private TextBox nameTextBox = new TextBox();
    private Label greetingLabel = new Label("Hello, GWT!");
    private VLayout layout = new VLayout();
    
    private SpreadJSComponent spreadjs1;
    private ReturnMsgEvent returnEvt1;
    
    private SpreadJSComponent spreadjs2;
    private ReturnMsgEvent returnEvt2;
    
    @Override
    public void onModuleLoad() {
        
//        layout.setID("main-test");
//        layout.setHeight100();
//        layout.setWidth100();
//        spreadjs1 = new SpreadJSComponent();
//        spreadjs1.setId("1");
//        spreadjs1.setHandler(new SheetHandler(){
//            public void cellClick(JavaScriptObject evt, JavaScriptObject args) {
//                log("test cellClick");
//            }
//        });
//        returnEvt1 = new ReturnMsgEvent();
//        returnEvt1.setSpreadjs(spreadjs1);
//        FormLoadListener.register(returnEvt1);
//        
//        
//        layout.addMember(spreadjs1.createWidget());
//        layout.addMember(greetingLabel);
//        layout.addMember(nameTextBox);
//        spreadjs2 = new SpreadJSComponent();
//        spreadjs2.setId("2");
//        spreadjs2.setHandler(new SheetHandler());
//        layout.addMember(spreadjs2.createWidget());
//        returnEvt2 = new ReturnMsgEvent();
//        returnEvt2.setSpreadjs(spreadjs2);
//        FormLoadListener.register(returnEvt2);
//        
//        
//        layout.draw();
        
        final AsyncCallback<String> callback = new AsyncCallback<String>() {
            @Override
            public void onFailure(Throwable caught) {
                greetingLabel.setText("ERROR!");
            }

            @Override
            public void onSuccess(String result) {
                greetingLabel.setText(result);
            }
        };

        nameTextBox.addKeyUpHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent keyUpEvent) {
                greetingService.greet(nameTextBox.getText(), callback);
            }
        });
        
        GWT.create(FormLoadListener.class);
        GWT.create(MyUtilityClass.class);
        
        
    }
    
}
